package com.project.game.entity;

import java.awt.Point;

import com.project.game.Input;
import com.project.game.gfx.Screen;
import com.project.game.map.Map;
import com.project.game.tile.Type;

public class Player extends Entity {

	public Player(int x, int y) {
		super(0, 0, 1, 2);
		this.x = x * 16;
		this.y = y * 16;
	}
	
	private double speed = 0.85;
	private double jumpSpeed = 4.0;
	private double airSpeedRatio = 0.6;
	private double accX = 0, accY = 0;
	private double gravity = 12.0/60.0;
	private double friction = 24.0/60.0;
	private double terminalVelocity = 165.0/60.0;
	private boolean inAir = false;
	
	Point[] collisionPointsX = new Point[92];
	Point[] collisionPointsY = new Point[92];
	
	public void tick(Map map, Input input) {
		if (input.left.down) {
			if (inAir) {
				accX -= speed * airSpeedRatio;
			} else accX -= speed;
		} if (input.right.down) {
			if (inAir) {
				accX += speed * airSpeedRatio;
			} else accX += speed;
		}
		if (input.space.down && !inAir) accY -= jumpSpeed;
		
		accY += gravity;
		
		if (accX < friction && accX > -friction) accX = 0;
		if (accX < -friction) accX += friction;
		if (accX > friction) accX -= friction;
		if (accX > terminalVelocity) accX = terminalVelocity;
		if (accX < -terminalVelocity) accX = -terminalVelocity;
		
		int xi = 0, yi = 0;
		for (int yy = 0; yy < 31; yy++) {
			for (int xx = 0; xx < 2; xx++) {
				collisionPointsX[xi] = new Point((int) (x + accX + xx * 15), (int) (y + yy));
				xi++;
				collisionPointsY[yi] = new Point((int) (x + xx * 15), (int) (y + accY + yy));
				yi++;
			}
		} for (int yy = 0; yy < 2; yy++) {
			for (int xx = 0; xx < 15; xx++) {
				collisionPointsX[xi] = new Point((int) (x + accX + xx), (int) (y + yy * 31));
				xi++;
				collisionPointsY[yi] = new Point((int) (x + xx), (int) (y + accY + yy * 31));
				yi++;
			}
		}
		
		System.out.println(xi + " " + yi);
		
		if (map.isInside(Type.SOLID, collisionPointsX)) accX = 0;
		if (map.isInside(Type.SOLID, collisionPointsY)) accY = 0;
		if (map.isInside(Type.LIQUID, collisionPointsX)) accX *= 0.8;
		if (map.isInside(Type.LIQUID, collisionPointsY)) accY *= 0.95;
		
		if (accY == 0) inAir = false; else inAir = true;
		
		if (inAir) x += .7 * accX; else x += accX;
		y += accY;
	}
	
	@Override
	public void render(Screen screen) {
		screen.drawEntity(id, (int) x, (int) y);
		
//		for (int i = 0; i < collisionPointsX.length; i++) screen.drawPoint(collisionPointsX[i].x, collisionPointsY[i].y);
//		for (int i = 0; i < collisionPointsY.length; i++) screen.drawPoint(collisionPointsX[i].x, collisionPointsY[i].y);
	}
}
