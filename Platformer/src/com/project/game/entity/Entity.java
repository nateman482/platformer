package com.project.game.entity;

import com.project.game.Input;
import com.project.game.gfx.Screen;
import com.project.game.map.Map;

public class Entity {

	public double x, y;
	public int[] id;
	
	public Entity(int id0, int id1, int id2, int id3) {
		this.id = new int[4];
		id[0] = id0;
		id[1] = id1;
		id[2] = id2;
		id[3] = id3;
	}
	
	public void tick(Map map, Input input) { }
	
	public void render(Screen screen) { }
}
