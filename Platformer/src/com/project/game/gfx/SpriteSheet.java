package com.project.game.gfx;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class SpriteSheet {

	public BufferedImage[] getSprites(String path) {
		BufferedImage sheet = null;
		BufferedImage[] sprites;
		
		try {
			sheet = ImageIO.read(SpriteSheet.class.getClassLoader().getResourceAsStream(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int rows = sheet.getHeight() / 16;
		int cols = sheet.getWidth() / 16;
		sprites = new BufferedImage[rows * cols];
		
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				sprites[(i * cols) + j] = sheet.getSubimage(
							i * 16,
							j * 16,
							16,
							16
						);
			}
		}
		
		return sprites;
	}
}
