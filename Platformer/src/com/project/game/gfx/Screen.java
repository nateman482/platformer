package com.project.game.gfx;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Screen {
	
	public final int width, height;
	public int xoffs, yoffs;
	private final Graphics g;
	private final BufferedImage[] tileSprites, entitySprites;
	
	public Screen(int width, int height, Graphics g) {
		this.width = width;
		this.height = height;
		this.g = g;
		
		tileSprites = new SpriteSheet().getSprites("img/tiles.png");
		entitySprites = new SpriteSheet().getSprites("img/entities.png");
	}
	
	public void drawTile(int id, int x, int y) {
		g.drawImage(tileSprites[id], x, y, null);
	}
	
	public void drawEntity(int[] id, int x, int y) {
		for (int xx = id[0]; xx < id[0] + id[2]; xx++) {
			for (int yy = id[1]; yy < id[1] + id[3]; yy++) {
				g.drawImage(entitySprites[(id[0] + xx) * 16 + id[1] + yy], (x + xx * 16) - xoffs, (y + yy * 16) - yoffs, null);
			}
		}
	}
	
	public void drawPoint(int x, int y) {
		g.fillRect(x - xoffs, y - yoffs, 1, 1);
	}
	
	public void setOffs(int xoffs, int yoffs) {
		this.xoffs = xoffs;
		this.yoffs = yoffs;
	}
}
