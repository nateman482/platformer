package com.project.game.map.tile;

import com.project.game.tile.Type;

public class WaterTile extends Tile {

	public WaterTile(int id) {
		super(id, Type.LIQUID);
	}
	
	@Override
	public boolean isLiquid() {
		return true;
	}
}
