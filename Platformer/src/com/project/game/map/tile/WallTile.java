package com.project.game.map.tile;

import com.project.game.tile.Type;

public class WallTile extends Tile {

	public WallTile(int id) {
		super(id, Type.SOLID);
	}
	
	@Override
	public boolean isSolid() {
		return true;
	}
}
