package com.project.game.map.tile;

import com.project.game.tile.Type;

public class GroundTile extends Tile {

	public GroundTile(int id) {
		super(id, Type.SOLID);
	}
	
	@Override
	public boolean isSolid() {
		return true;
	}
}
