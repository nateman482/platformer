package com.project.game.map.tile;

import com.project.game.tile.Type;

public class AirTile extends Tile {
	
	public AirTile(int id) {
		super(id, Type.AIR);
	}
}
