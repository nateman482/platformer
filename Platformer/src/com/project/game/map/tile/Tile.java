package com.project.game.map.tile;

import com.project.game.tile.Type;

public class Tile {
	
	public static Tile[] tiles = new Tile[64];
	
	public static Tile ground = new GroundTile(0);
	public static Tile air = new AirTile(1);
	public static Tile wall = new WallTile(2);
	public static Tile water = new WaterTile(3);
	
	public int id;
	public Type type;
	
	public Tile(int id, Type type) {
		this.id = id;
		this.type = type;
		tiles[id] = this;
	}
	
	public boolean isSolid()  {
		return false;
	}
	
	public boolean isLiquid() {
		return false;
	}
	
	public boolean isInside(Type type) {
		if (type == this.type) return true;
		else return false;
	}
}
