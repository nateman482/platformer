package com.project.game.map;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import com.project.game.Input;
import com.project.game.entity.Entity;
import com.project.game.entity.Player;
import com.project.game.gfx.Screen;
import com.project.game.map.tile.Tile;
import com.project.game.tile.Type;

public class Map {

	private final int mapWidth, mapHeight;
	private int[][] tiles;

	private List<Entity> entities = new ArrayList<Entity>();
	private Player player;
	
	public final Input input;

	public Map(int mw, int mh, Input input) {
		this.mapWidth = mw + 2;
		this.mapHeight = mh;
		this.input = input;

		tiles = new int[mapWidth][mapHeight];
		for (int x = 0; x < mapWidth; x++) {
			for (int y = 0; y < mapHeight; y++) {
				setTile(Tile.air, x, y);
				if (mapHeight - y < 4) {
					setTile(Tile.ground, x, y);
				} if (x == 0 || x == mapWidth - 1) {
					setTile(Tile.wall, x, y);
				} if (x == 2 && y == mapHeight - 5) {
					setTile(Tile.ground, x, y);
				} if (x > 10 && x < 14 && y == mapHeight - 3) {
					setTile(Tile.water, x, y);
				}
			}
		}

		setTile(Tile.ground, 5, 5);
		
		player = new Player(2, 2);
		entities.add(player);
	}

	private boolean setTile(Tile tile, int x, int y) {
		if (x < mapWidth && y < mapHeight && x >= 0 && y >= 0) {
			tiles[x][y] = tile.id;
			return true;
		} else {
			System.out.println("ERROR: Tile placed out of bounds at " + x + ", " + y);
			return false;
		}
	}

	private int getTile(int x, int y) {
		return tiles[x][y];
	}

	public boolean isInsideSolid(int x, int y) {
		if (Tile.tiles[getTile(x / 16, y / 16)].isSolid()) {
			return true;
		}
		return false;
	}
	
	public boolean isInsideLiquid(int x, int y) {
		if (Tile.tiles[getTile(x / 16, y / 16)].isLiquid()) {
			return true;
		}
		return false;
	}
	
	public boolean isInside(Type type, Point[] points) {
		for (int i = 0; i < points.length; i++) {
			if (Tile.tiles[getTile(points[i].x / 16, points[i].y / 16)].isInside(type)) {
				return true;
			}
		}
		return false;
	}

	public void tick(Input input) {
		for (Entity entity : entities) {
			entity.tick(this, input);
		}
	}

	private int xoffs, yoffs;
	
	public void drawGame(Screen screen) {
		xoffs = (int) (player.x) - screen.width / 2 + 8;
		yoffs = (int) (player.y) - screen.height / 2 + 2;
		screen.setOffs(xoffs, yoffs);
		
		for (int x = 0; x < mapWidth; x++) {
			for (int y = 0; y < mapHeight; y++) {
				screen.drawTile(tiles[x][y], x * 16 - xoffs, y * 16 - yoffs);
			}
		}
		
		for (Entity entity : entities) {
//			screen.drawEntity(entity.id, (int) (entity.x) - xoffs, (int) (entity.y) - yoffs);
			entity.render(screen);
		}
	}
}
